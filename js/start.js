/* global SystemJS */

SystemJS.config({
    baseURL: './node_modules',
    map: {
        'plugin-babel': 'systemjs-plugin-babel/plugin-babel.js',
        'systemjs-babel-build': 'systemjs-plugin-babel/systemjs-babel-browser.js'
    },
    packageConfigPaths: [
        '*/package.json',
    ],
    packages: {
        'ultradom': {
            main: 'ultradom.m.js',
        },
        'dredge': {
            main: 'src/index.js',
        }
    },
    transpiler: 'plugin-babel'
});

SystemJS.import('./js/main.js');
