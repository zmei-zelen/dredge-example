import {h} from 'ijk';
import {render} from 'ultradom';
import {dredge} from 'dredge';

const tree = h('name','attributes','children');

const state = {
    imageUrl: 'https://picsum.photos/600/?random',
    loading: false,
    showTooltip: true
}

const imageLoaded = src => state => ({imageUrl: src, loading: false});

const randomImage = (state, {dispatch}) => {
    const image = new Image();
    image.onload = () => setTimeout(() => dispatch(imageLoaded(image.src)), 3000);
    image.src = 'https://picsum.photos/600/?random&r=' + Math.random().toString().substr(2);
    
    return {loading: true, showTooltip: false};
}

const view = (state, action) => ['div', {class: 'content'}, [
    ['a', {class: 'random-image', onclick: action(randomImage), }, [
        ['img', {src: state.imageUrl, alt: 'Random image'}],
        ['div', {class: `tooltip ${!state.showTooltip && 'hidden'}`}, 'Click to get a random image!'],
    ]],
    ['div', {class: `lds-ripple ${!state.loading && 'hidden'}`}, [['div'], ['div']]]
]];

const renderer = (state, action) => render(tree(view(state, action)), document.body);

dredge(state).tap(renderer);
