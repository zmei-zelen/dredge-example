
# Dredge Example
> A simple web application that displays random images. A demonstration of using the Dredge application framework.

## Prerequisites
Any web server to host the application and [npm](https://www.npmjs.com/) to install the dependencies.

## Installation
```
git clone git@bitbucket.org:zmei-zelen/dredge-example.git
cd dredge-example
npm install
```

## Running

Any web server can be used to run the application. A simple way would be to use the server from [Python](https://www.python.org/), assuming it is installed:
```
python -m SimpleHTTPServer    # python 2.x
```
or
```
python -m http.server         # python 3.x
```
Then the application can be accessed on http://localhost:8000/
